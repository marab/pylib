import csv
# ----------------------------------
# csv helper

def read(filename, delimiter=','):
    rows = []
    import csv
    with open(filename, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=delimiter)
        for row in spamreader:
            rows.append(row)
    return rows

def write(filename, rows):
    with open(filename, 'w') as csvfile:
        writer = csv.writer(csvfile);
        for row in rows:
            writer.writerow(row)

def keep_columns(selected, values):
    selectedIndex = [];
    headers = values[0];
    for header in selected:
        index = headers.index(header)
        selectedIndex.append(index)
    newValues = []
    for row in values:
        newRow = []
        for index in selectedIndex:
            newRow.append(row[index])
        newValues.append(newRow)
    return newValues

def get_header_d(rows):
    i = 0;
    headers = rows[0];
    header_d = {}
    while i < len(headers) :
        header_d[headers[i]] = i;
        i = i + 1
    return header_d;
