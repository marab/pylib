#!/usr/bin/env python
# -*- coding: utf-8 -*-
import xlrd
XLRD_USE_ROUND = False
XLRD_ROUND = 8


class XlrdUtil :
	@staticmethod
	def to_read(uu):
		if isinstance(uu, str):
			uu = uu
			#uu = str(uu)
			uu = uu.encode('utf-8')
			#uu = uu.decode('ascii','ignore')
			return uu
		elif isinstance(uu, bytes):
			uu = uu
		try :
			uu = float(uu)
			if XLRD_USE_ROUND :
				uu = round(float(uu),XLRD_ROUND)
			if int(uu) == uu :
				uu = int(uu)
		except :
			pass
		return uu

	@staticmethod
	def to_read_l(uu_l) :
		return [XlrdUtil.to_read(x) for x in uu_l]

	def __init__(self,filename, formatting_info=False) :
		self.open_workbook(filename, formatting_info)
		self.formatting_info = formatting_info

	def open_workbook(self,filename, formatting_info=False) :
		self.formatting_info = formatting_info
		self.wb = xlrd.open_workbook(filename, formatting_info=formatting_info)
		self.format_mapping = {}

	def sheet_names(self) :
		return self.wb.sheet_names()

	def open_sheet(self, sheetname) :
		self.format_mapping[sheetname] = {}
		self.active_sheetname = sheetname
		self.active_sheet = self.wb.sheet_by_name(sheetname)
		self.sheet_format_array = [list(self.active_sheet.ncols*[None]) for x in range(0,self.active_sheet.nrows)]
		return self.active_sheet

	def read_cell_format(self, rowx, colx) :
		f_d = {}
		#

		cell = self.active_sheet.cell(rowx,colx)
		fmt = self.wb.xf_list[cell.xf_index]
		#fmt.dump()
		#
		rgb = self.wb.colour_map[fmt.background.pattern_colour_index]
		if rgb != None :
			f_d['bg_color'] = '#%02x%02x%02x' % rgb
		##
		fl = self.wb.font_list[fmt.font_index]
		font_color =  self.wb.colour_map[fl.colour_index]
		if font_color != None :
			f_d['font_color'] = '#%02x%02x%02x' % font_color

		fl = self.wb.font_list[fmt.font_index]
		if fl.underline_type in [1,2] :
			f_d['underline'] = 1
		if fl.italic :
			f_d['italic'] = 1
		if fl.weight == 700 :
			f_d['bold'] = 1


		self.sheet_format_array[rowx][colx] = f_d
		# bold, italic , underline, font, bg_color, font_color



		#--


	def read_sheet_row_format(self, rowx, start_colx=0, end_colx=None) :
		if end_colx == None :
			end_colx=self.active_sheet.ncols
		for colx in range(start_colx, end_colx) :
			self.read_cell_format(rowx,colx)

	def read_sheet_row(self, rowx, start_colx=0, end_colx=None) :
		if self.formatting_info :
			self.read_sheet_row_format(rowx, start_colx, end_colx)
		return XlrdUtil.to_read_l(self.active_sheet.row_values(rowx, start_colx, end_colx))

	def read_sheet_col_format(self, colx, start_rowx=0, end_rowx=None) :
		if end_rowx == None :
			end_rowx=self.active_sheet.nrows
		for rowx in range(start_rowx, end_rowx) :
			self.read_cell_format(rowx,colx)

	def read_sheet_col(self, colx, start_rowx=0, end_rowx=None) :
		if self.formatting_info :
			self.read_sheet_col_format(colx, start_rowx, end_rowx)
		return XlrdUtil.to_read_l(self.active_sheet.col_values(colx, start_rowx, end_rowx))

	@staticmethod
	def static_read_info_line(filename, sheetname, rowx_l) :
		xlrd = XlrdUtil(filename)
		xlrd.open_sheet(sheetname)
		info_line = xlrd.read_info_line(rowx_l)
		return info_line

	@staticmethod
	def static_read_info_line_d(filename, sheetname, rowx_l) :
		xlrd = XlrdUtil(filename)
		xlrd.open_sheet(sheetname)
		info_line = xlrd.read_info_line(rowx_l)

		info_line = sorted(info_line)

		print("===> [XLRD Util] There is the basic dictionary to use date in the excel : ")
		for x in info_line :
			print("'"+x+"' : ('do_ignore', do_ignore),")

	def read_info_line(self, rowx_l, start_colx=0, end_colx=None) :
		info_l = ["" for tmp in range(0,self.active_sheet.ncols)]
		for rowx in rowx_l :
			row_v_l = XlrdUtil.to_read_l(self.active_sheet.row_values(rowx, start_colx, end_colx))
			for i in range(0, len(row_v_l)) :
				if row_v_l[i] == "" :
					row_v_l[i] = old_value
				else :
					old_value = row_v_l[i]

				if rowx > rowx_l[0] :
					info_l[i] = info_l[i]+"#"+row_v_l[i]
				else :
					info_l[i] = row_v_l[i]
		return info_l

	def read_sheet_row_dict(self, rowx_l, start_colx=0, end_colx=None) :
		info_l = ["" for tmp in range(0,self.active_sheet.ncols)]
		for rowx in rowx_l :
			row_v_l = XlrdUtil.to_read_l(self.active_sheet.row_values(rowx, start_colx, end_colx))
			old_value = ""
			for i in range(0, len(row_v_l)) :
				if row_v_l[i] == "" :
					row_v_l[i] = old_value
				else :
					old_value = row_v_l[i]

				if rowx > rowx_l[0] :
					info_l[i] = info_l[i]+"#"+row_v_l[i]
				else :
					info_l[i] = row_v_l[i]
		start_row = rowx_l[-1]+1
		sheet_array_l = self.sheet_to_row_array(start_row)
		info_l = [str(x) for x in info_l]
		#res_array
		res_l = []
		for sheet_array in sheet_array_l :
			d = {}
			for k,v in zip(info_l, sheet_array) :
				if v != "" :
					d[k] = v
			res_l.append(d)
		return res_l

	def read_sheet_row_dict_dict(self, rowx_l, id_field, start_colx=0, end_colx=None) :
		res_dl = self.read_sheet_row_dict(rowx_l,start_colx, end_colx)
		info_line = self.read_info_line(rowx_l)
		ind = info_line.index(id_field)
		res_dd = {}
		for res_d in res_dl :
			res_dd[res_d[id_field]] = res_d

		return res_dd


	def sheet_to_row_array(self, start_rowx=0) :
		row_array = []
		for rowx in range(start_rowx, self.active_sheet.nrows) :
			row_array.append(self.read_sheet_row(rowx))
		return row_array

	def sheet_to_col_array(self) :
		col_array = []
		for colx in range(0, self.active_sheet.ncols) :
			col_array.append(self.read_sheet_col(colx))
		return col_array

	def sheet_to_col_dict(self,start_rowx=0) :
		col_d = {}
		for rowx in range(start_rowx, self.active_sheet.nrows) :
			row_l = self.read_sheet_row(rowx)
			if len(row_l)> 0 :
				value = [x for x in row_l[1:] if x != ""]
				if len(value) > 0 :
					col_d[row_l[0]] = value
		return col_d

	def workbook_to_row_array_dict(self) :
		d = {}
		d_format = {}
		for sheetname in self.sheet_names() :
			self.open_sheet(sheetname)
			d[sheetname] = self.sheet_to_row_array()
			d_format[sheetname] = self.sheet_format_array

		self.row_array_dict = d
		self.row_array_format_dict = d_format
		return d

	def workbook_to_row_array_dict_dict(self) :
		d = {}
		d_format = {}
		for sheetname in self.sheet_names() :
			self.open_sheet(sheetname)
			d[sheetname] = self.read_sheet_row_dict(rowx_l=[0])
			d_format[sheetname] = self.sheet_format_array

		self.row_array_dict = d
		self.row_array_format_dict = d_format
		return d

	def workbook_to_col_array_dict_dict(self) :
		d = {}
		d_format = {}
		for sheetname in self.sheet_names() :
			self.open_sheet(sheetname)
			d[sheetname] = self.sheet_to_col_dict()
			d_format[sheetname] = self.sheet_format_array

		self.row_array_dict = d
		self.row_array_format_dict = d_format
		return d

	def workbook_to_arrayl(self) :
		array_l = []
		for sheetname in self.sheet_names() :
			self.open_sheet(sheetname)
			a = self.sheet_to_row_array()
			a[0].insert(0,"info.sheetname")
			for row in a[1:] :
				row.insert(0,sheetname)
			array_l.append(a)
		return array_l
