#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xlsxwriter

import my_lib.object_pdl as opdl_lib

class XlrwUtil :
	@staticmethod
	def to_write(uu) :
		if isinstance(uu, unicode):
			#uu = str(uu)
			uu = uu.decode('utf-8')
			#uu = uu.decode('ascii','ignore')
			return uu
		elif isinstance(uu, str):
			uu = uu.decode("utf-8")
		try :
			uu = float(uu)
			if int(uu) == uu :
				uu = int(uu)
		except :
			pass
		return uu

	@staticmethod
	def to_write_l(uu_l) :
		return [XlrwUtil.to_write(x) for x in uu_l]

	def __init__(self, filename) :
		self.wb = xlsxwriter.Workbook(filename, {'default_date_format':'dd/mm/yy'})
		self.sheet_count = 0
		self.sheet_d = {}


	def add_sheet(self, sheetname=None) :
		if type(sheetname) != type(None) :
			if sheetname not in self.sheet_d :
				self.sheet_count += 1
				self.sheet_d[sheetname] = self.wb.add_worksheet(sheetname)
		#else :
		#	self.sheet_count += 1
		#	sheetname = "Sheet"+str(self.sheet_count)
		#	self.sheet_d[sheetname] = self.wb.add_worksheet()
			self.active_sheet = self.sheet_d[sheetname]
		return self.active_sheet

	def write_cell(self, rowx, colx, value, format=None) :
		#
		if isinstance(value,list) :
			value = " | ".join(value)
		value = XlrwUtil.to_write(value)
		self.active_sheet.write(rowx,colx,value,format)

	def write_row(self, rowx, row_value, col_start=0, format=None) :	
		colx = col_start
		if type(format) == type(None) :
			format = [self.wb.add_format()]*len(row_value)
		elif isinstance(format,dict) :
			format = [self.wb.add_format(format)]*len(row_value)
		elif len(format) != len(row_value) :
			format = [self.wb.add_format()]*len(row_value)
		elif type(format) == list :
			format = [self.wb.add_format(x) for x in format]
		else :	
			format = [self.wb.add_format(format)]*len(row_value)
		for value,format_c in zip(row_value,format) :
			self.write_cell(rowx,colx,value,format_c)
			colx +=1

	def write_col(self, colx, col_value, row_start=0, format=None) :
		rowx = row_start
		if type(format) == type(None) :
			format = [self.wb.add_format()]*len(col_value)
		elif type(format) == list :
			format = [self.wb.add_format(x) for x in format]
		else :
			format = [self.wb.add_format(format)]*len(col_value)
		
		for value,format_c in zip(col_value,format) :
			self.write_cell(rowx,colx,value,format_c)
			rowx +=1

	def write_col_array(self, array_value, row_start=0,col_start=0,   format=None) :
		colx = col_start
		if type(format) == type([]) :
			for col_value,format_x in zip(array_value, format) :
				self.write_col(colx, col_value, format = format_c, row_start=row_start)
				colx+=1
		else :
			for col_value in array_value :
				self.write_col(colx, col_value, format = format, row_start=row_start)
				colx+=1

	def write_row_array(self, array_value, row_start=0, col_start=0, sheet_format_array=None) :
		rowx = row_start
		for row_value in array_value :
			sheet_format_row = None
			if sheet_format_array != None :
				if rowx < len(sheet_format_array) :
					sheet_format_row = sheet_format_array[rowx]
			self.write_row(rowx, row_value, format=sheet_format_row, col_start=col_start)
			rowx+=1
			

	def write_row_arrayd(self, array_value_dl, row_start=0, col_start=0, sheet_format_array=None, header_l=None,action_d = {}) :
		rowx = row_start
		linex = 0
		if len(array_value_dl) < 1 :
			return
		info_line = array_value_dl[0].keys()
		if header_l is not None :
			info_line = header_l

		if "sort" in action_d :
			opdl_lib.SortUtil.sort(array_value_dl, action_d["sort"]["key"],action_d["sort"]["reverse"])
		if "col_width" in action_d :
			for (k,v) in action_d["col_width"].iteritems() :
				#print k, v, info_line.index(k)
				self.active_sheet.set_column(info_line.index(k),info_line.index(k),v)
		if sheet_format_array == None :
			self.write_row(rowx,info_line, col_start=col_start, format = action_d.get("header_format",None))
			rowx+=1
			for row_value in array_value_dl:
				self.write_row(rowx, [row_value[k] for k in info_line], col_start=col_start, format = [action_d.get("col_format", {"col_format" : None}).get(k,{}) for k in info_line])
				rowx+=1
		else :
			self.write_row(rowx,info_line, format=action_d.get("header_format",sheet_format_array[rowx]), col_start=col_start)
			rowx+=1
			for row_value in array_value_dl :

				self.write_row(rowx, [row_value[k] for k in info_line], format=sheet_format_array[rowx], col_start=col_start)
				rowx+=1
		#
		if  action_d.get("auto_filter","no") == "yes" :
			self.active_sheet.autofilter(0,0,len(array_value_dl),len(info_line))

	def write_row_array_dict(self, dic, format_dic = None) :
		for sheetname, row_array in sorted(dic.items()) :
			self.add_sheet(sheetname)
			sheet_format_array = None
			if format_dic : 
				if sheetname in format_dic :
					sheet_format_array = format_dic[sheetname]
			self.write_row_array(row_array, sheet_format_array=sheet_format_array)

	def write_row_array_dict_dict(self, dic, format_dic = None, header_l=None,action_d = {}) :
		for sheetname, row_array in sorted(dic.items()) :
			self.add_sheet(sheetname)
			if format_dic != None :
				sheet_format_array = None
				if sheetname in format_dic :
					sheet_format_array = format_dic[sheetname]
				self.write_row_arrayd(row_array, sheet_format_array=sheet_format_array,header_l=header_l,action_d =action_d)
			else :
				self.write_row_arrayd(row_array, header_l=header_l, action_d =action_d)

	def close(self) :
		self.wb.close()



if __name__ == "__main__" :
	print "Xlsxwriter"
	filename = "write_test.xlsx"
	XLW = XlrwUtil(filename)
	XLW.add_sheet("test")
	XLW.write_cell(0,0, "TEST BEGIN !")
	XLW.write_row(1,["a","b","c","", "d"])
	XLW.write_col(3,["a","b","c","", "d"], row_start=1)

	array = [[1,2,3],[6,4,5]]
	XLW.write_col_array(array,7,2)

	XLW.write_row_array(array, 10, 1)

	d = {"Dic1" : [[1,2,3,4,5]],
		"Dic2" : [[1,2,3,4,5]]}
	XLW.write_row_array_dict(d)
	XLW.close()

	from xlrd_util import *
	filename_in = "read_test.xls"
	XLR = XlrdUtil(filename_in, formatting_info=True)

	XLR.workbook_to_row_array_dict()
	filename = "copy_test.xlsx"
	XLW = XlrwUtil(filename)
	#print XLR.row_array_dict
	#print XLR.row_array_format_dict
	XLW.write_row_array_dict(XLR.row_array_dict, XLR.row_array_format_dict)
